local sensorInfo = {
	name = "CmdPosition",
	desc = "get commander position",
	author = "PepeAmpere",
	date = "2019-04-20",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	if #units > 0 then
		local ID = units[1]
		local x,y,z = Spring.GetUnitPosition(ID)
		return
		{
					X = x,
					Z = z
	  }
	end
end



